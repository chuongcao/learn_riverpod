import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/constants.dart';

final prefProvider = FutureProvider<SharedPreferences>((ref) async {
   final pref = await SharedPreferences.getInstance();
   return pref;
});

class CounterNotifier extends StateNotifier<int> {
   static final provider = StateNotifierProvider<CounterNotifier, int>((ref) {
      final pref = ref.watch(prefProvider).maybeWhen(
         data: (data) => data,
         orElse: () => null
      );

      return CounterNotifier(pref);
   });

   CounterNotifier(
      this.pref
   ): super(pref?.getInt(PrefKeys.kCounter) ?? 0);

   final SharedPreferences? pref;

   Future<void> increment() async {
      state++;
      pref?.setInt(PrefKeys.kCounter, state);
   }

   Future<void> decrement() async {
      state--;
      pref?.setInt(PrefKeys.kCounter, state);
   }
}
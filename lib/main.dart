import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'providers/counter.provider.dart';

void main() {
   runApp(
      const ProviderScope(
         child: MyApp()
      )
   );
}

class MyApp extends ConsumerWidget {
   const MyApp({Key? key}) : super(key: key);

   @override
   Widget build(BuildContext context, WidgetRef ref) {
      final counter = ref.watch(CounterNotifier.provider);

      Future<void> increPressed() async {
         final incre = ref.watch(CounterNotifier.provider.notifier);
         await incre.increment();
      }

      Future<void> decrePressed() async {
         final incre = ref.watch(CounterNotifier.provider.notifier);
         await incre.decrement();
      }

      return MaterialApp(
         title: 'Flutter Demo',
         home: Scaffold(
            body: Column(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                  Text(counter.toString()),
                  Row(
                     mainAxisAlignment: MainAxisAlignment.center,
                     children: [
                        TextButton(
                           child: const Text("Incre"),
                           onPressed: () => increPressed(),
                        ),
                        TextButton(
                           child: const Text("Decre"),
                           onPressed: () => decrePressed(),
                        ),
                     ],
                  )
               ],
            ),
         )
      );
   }
}